

class Person; end

class Client < Person; end

class BusinessClient < Client
  def company_name
    'Company Name Here'
  end
end

a = Class.new(BusinessClient)
# <Class:0x007ffa5b142d48>

class CompanyClient < a;

CompanyClient.new.company_name
=> "Company Name Here"

CompanyClient.ancestors
=> [CompanyClient, #<Class:0x007ffa5b142d48>,
  BusinessClient, Client, Person,
  Object, Kernel, BasicObject
]


class Transaction
  def self.call(options = {})
    new(options).call
  end

  attr_reader :txid

  def initialize(options = {})
    @txid = "TX123"
  end
  def call; end

  def my_super_puper_method(name)
    p 'asdsadsa'
  end
end

class Withdraw < Transaction
  attr_reader :source_account, :target_account

  def initialize(options = {})
    @txid = "WITHDRAW#{super}"
    @source_account = options.fetch(:source_account)
    @target_account = options.fetch(:target_account)
    @amount = options.fetch(:amount)
  end

  def call
    source_account.sub_funds(amount)
    source_account.add_funds(amount)
  end

  def my_super_puper_method(name, last_name)
    super(name)
    p 'my super logic here'
  end

  def withdraw; end
end

Withdraw.new(options).txid
=> "WITHDRAWTX123"

Withdraw.ancestors
=> [Withdraw, Transaction, Object, Kernel, BasicObject]


