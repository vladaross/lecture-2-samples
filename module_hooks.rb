
module Logging
  def log(message)
    # log in a file
  end
end

module Debug
  def log(message)
    # debug output
  end
end

class Service
  extend Logging
  extend Debug
end

service.is_a?(Atm)

Service.new.log('my message here')

Service.ancestors # [Service, Debug, Logging, Object, ...]

# extend

module AtmConfiguration
  module ClassMethods
    def config
      @@config ||= Yaml.load_file('config.yml')
    end

    def accounts
      config[:accounts]
    end

    def banknotes
      config[:banknotes]
    end
  end

  def self.prepended(base)
    base.extend(ClassMethods)
  end

  def calculate_available_banknotes
    self.class.banknotes.each do

    end
  end
end

class Pivorak
  include Logging
end

# prepend

module ServiceDebugger
  def run(args)
    puts "Service run start: #{args.inspect}"
    result = super
    puts "Service run finished: #{result}"
  end
end

class Service
  prepend ServiceDebugger

  # perform some real work
  def run(args)
    args.each do |arg|
      sleep 1
    end
    {result: "ok"}
  end
end

Service.ancestors
=> [ServiceDebugger, Service, Object, ...]