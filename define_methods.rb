

class Company

  # def sub_funds
  %w(sub_funds add_funds).each do |method_name|
    define_method(method_name) do |amount|
      # sub funds
      account.send(method_name, amount)
    end
  end

  # def self.lock_accounts
  define_singleton_method(:lock_accounts) do
    # lock accounts
  end
end



class Withdraw
  undef_method(:withdraw)
end

=> NameError: undefined method `withdraw` for class `Withdraw`

class Withdraw
  remove_method(:withdraw)
end

=> NameError: method `withdraw' not defined in Withdraw